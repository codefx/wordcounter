﻿using Microsoft.Extensions.Caching.Memory;
using System;
using WordCounter.Tools.Implements;

namespace WordCounter.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Paths
            var fileName = "myfile";
            var sourceFilePath = $"../../../Downloads/{fileName}.txt";
            var targetFilePath = "../../../ResultXmls/words.xml";
        start:
            var thefileURL = @"http://www.gutenberg.org/files/2701/2701-0.txt";
            #endregion

            #region File Naming
            
            Console.WriteLine("please enter the file full url, if you not enter an url address, the default location will tried");
            var input = Console.ReadLine();
            if (input != string.Empty)
            {
                thefileURL = input;
            }
            Console.WriteLine("Please enter a file name you wish..");
            var nameInput = Console.ReadLine();
            if (nameInput != string.Empty)
            {
                fileName = nameInput;
            }
            sourceFilePath = $"../../../Downloads/{fileName}.txt";
            #endregion

            Console.WriteLine("Starting process .... please wait...");

            #region File downloading     

            var fld = new FileDownloader();
            var success = fld.DownloadFile(thefileURL, sourceFilePath, 90000); // 90 seconds, in milisecond type.
            if (success.Item1) Console.WriteLine("file downloaded : " + success);
            if (success.message=="2")
            {
                Console.WriteLine("\nplease enter a valid url.  or an other problem occured..");
                goto start;
            }

            #endregion

            #region Word Counting and  Data converting to Dictionary

            WordProcess wp = new WordProcess(new CacheMemoryService(new MemoryCache(new MemoryCacheOptions())));
            var dict = wp.CountWords(sourceFilePath);
            Console.WriteLine($"the file has {dict.Count} different words.");
            #endregion

            Console.WriteLine("Please enter a key to continue the process");
            Console.ReadKey();

            #region Dictionary Converting to Xml and saving

            var xmlConvertResult = wp.ConvertToXml(dict, targetFilePath);
            if (xmlConvertResult) Console.WriteLine("Done  - file converted to xml: " + xmlConvertResult);
            #endregion

            Console.WriteLine("Please enter a key to quit...");
            Console.ReadKey();

        }
        
    }
}
