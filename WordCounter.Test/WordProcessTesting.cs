using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordCounter.Tools.Implements;
using WordCounter.Tools.Interface;

namespace WordCounter.Test
{
    [TestClass]
    public class WordProcessTesting
    {
        #region Paths
           
        string mainTxtFile = "../../../../WordCounter.ConsoleApp/Downloads/myfile.txt";
        string txtFilePath = "../../../../WordCounter.Test/ResultFiles/testTxtFile.txt";
        string xmlFilePath = "../../../../WordCounter.ConsoleApp/ResultXmls/words.xml";
        string xmlTestFile = "../../../../WordCounter.Test/ResultFiles/words.xml";
        string url = "http://www.gutenberg.org/files/2701/2701-0.txt"; // "Moby Dick" 's url
        #endregion        

        #region FileDownloader testing
       
        [TestMethod, Priority(1)]
        public void DownloadFileTest()
        {
            FileDownloader fld = new FileDownloader();
            var x = fld.DownloadFile(url, txtFilePath, 60000);
            //Assert.AreEqual(true, x.Item1);  // if first time downloading,
            Assert.AreEqual(false, x.Item1);  // the file already downloaded
        }
        #endregion

        #region WordProcess Testing

        IWordProcess wp = new WordProcess(new CacheMemoryService(new MemoryCache(new MemoryCacheOptions())));

        [TestMethod, Priority(2)]
        public void CountWordsTest()
        {
            var expectedWordCounts = 18518;
            var result = wp.CountWords(mainTxtFile).Count;
            Assert.AreEqual(expectedWordCounts, result);
        }

        [TestMethod, Priority(3)]
        public void ConvertToXmlTest()
        {
            var dict = wp.CountWords(mainTxtFile);
            var result = wp.ConvertToXml(dict, xmlFilePath);
            var result2 = wp.ConvertToXml(dict, xmlTestFile); 
            Assert.AreEqual(true, result);
        }

        [TestMethod, Priority(4)]
        public void GetTopXListTest()
        {
            var main = wp.GetTopXListOfWord(12, xmlFilePath, dataRefresh: false);
            var test = wp.GetTopXListOfWord(12, xmlTestFile, dataRefresh: false);
            Assert.AreEqual(test, main);
        }

        #endregion
    }
}
