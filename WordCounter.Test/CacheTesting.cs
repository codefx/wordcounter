﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using WordCounter.Tools.Implements;
using WordCounter.Tools.Interface;

namespace WordCounter.Test
{
    [TestClass]
    public class CacheTesting
    {
        ICacheService cacheService;

        [TestMethod, Priority(1)]
        public void SetAndGetCacheKeyTest()
        {
            cacheService = new CacheMemoryService(new MemoryCache(new MemoryCacheOptions()));
            cacheService.Set("TestKey", "TestCacheObject");
            var cacheValue = cacheService.Get<string>("TestKey");  //Set method is "void", no return.
            Assert.AreEqual("TestCacheObject", cacheValue);
        }

        [TestMethod, Priority(2)]
        public void GetOrAddCacheKeyTest()
        {
            Func<string> dataFunc = () => { return "Testing goa key"; };  // Get Or Add --> goa
            cacheService = new CacheMemoryService(new MemoryCache(new MemoryCacheOptions()));
            var goaStr = cacheService.GetOrAdd("Test2Key",dataFunc,false,50);
            var goaStrTrue = cacheService.GetOrAdd("Test2Key", dataFunc, true, 50);

            Assert.AreEqual("Testing goa key", goaStr);
            Assert.AreEqual("Testing goa key", goaStrTrue);
        }

        [TestMethod, Priority(3)]
        public void RemoveCacheKeyTest()
        {
            cacheService = new CacheMemoryService(new MemoryCache(new MemoryCacheOptions()));
            cacheService.GetOrAdd("Test3Key", ()=> { return "will removed Key"; }, false, 50);
            cacheService.Remove("Test3Key");
            var removeKey = cacheService.Get<string>("Test3Key");
            Assert.AreEqual(removeKey,null);
        }
    }
}
