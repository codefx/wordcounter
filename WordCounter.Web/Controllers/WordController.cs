﻿using Microsoft.AspNetCore.Mvc;
using WordCounter.Tools.Interface;


namespace WordCounter.Web.Controllers
{
    public class WordController : Controller
    {
        private readonly IWordProcess _wordProcess;

        public WordController(IWordProcess wordProcess)
        {
            _wordProcess = wordProcess;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult List(bool dataRefresh = false) //datarefresh is for caching parameter
        {
            var model = _wordProcess.GetTopXListOfWord(10, "..//WordCounter.ConsoleApp/ResultXmls/words.xml", dataRefresh);
            return View(model);
        }
    }
}