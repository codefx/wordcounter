﻿

namespace WordCounter.Tools.Interface
{
    public interface IFileDownloader
    {
        (bool, string message) DownloadFile(string _url, string _fullPathWhereToSave, int timeout = 100000);
    }
}
