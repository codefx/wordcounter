﻿using System;

namespace WordCounter.Tools.Interface
{
    public interface ICacheService
    {
        T Get<T>(string cacheKey);
        void Set<T>(string cacheKey, T obj, int cacheDurationSecond = 300);
        T GetOrAdd<T>(string cacheKey, Func<T> func, bool cacheRefresh = false, int cacheDurationSecond = 300);
        void Remove(string cacheKey);
    }
}
