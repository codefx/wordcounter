﻿using System.Collections.Generic;
using WordCounter.Tools.Model;

namespace WordCounter.Tools.Interface
{
    public interface IWordProcess
    {
        IDictionary<string, int> CountWords(string sourceFilePath);
        bool ConvertToXml(IDictionary<string, int> wordDict, string targetFilePath);
        Words GetWordsFromXmlFile(string sourceFilePath);
        List<Word> GetTopXListOfWord(int count, string sourceFilePath, bool dataRefresh = false);
    }
}