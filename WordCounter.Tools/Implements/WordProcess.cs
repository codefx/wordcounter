﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WordCounter.Tools.Interface;
using WordCounter.Tools.Model;


namespace WordCounter.Tools.Implements
{
    public class WordProcess : IWordProcess
    {
        private readonly ICacheService _cacheService;

        public WordProcess(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        /// <summary>
        /// this method reads a text file, counts text file's words and each word's frequency,  and adds them to a Concurrent Dictionary.
        /// </summary>
        /// <param name="sourceFilePath">the path which file to read</param>
        /// <returns> this method returns Concurrent Dictionary</returns>
        public IDictionary<string, int> CountWords(string sourceFilePath)
        {
            if (File.Exists(sourceFilePath))
            {
                string pattern = @"(([A-Za-z0-9])+([A-Za-z0-9\-]))+";
                var result = new ConcurrentDictionary<string, int>();

                Parallel.ForEach(File.ReadLines(sourceFilePath, Encoding.UTF8), line =>
                {
                    var words = line.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var word in words)
                    {
                        var newWord = WordFixer(word, pattern);
                        if (!string.IsNullOrWhiteSpace(newWord))
                            result.AddOrUpdate(newWord.ToLower(new CultureInfo("en-US")), 1, (k, v) => v + 1);
                    }
                });
                return result;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// This method converts a dictionary type that derived from IDictionary, to XML type format and saves it to targetPath.
        /// </summary>
        /// <param name="wordDict">dictionary that will be converted </param>
        /// <param name="targetFilePath">the path where will saved </param>
        /// <returns>this method returns bool, true or false</returns>
        public bool ConvertToXml(IDictionary<string, int> wordDict, string targetFilePath)
        {
            if (!File.Exists(targetFilePath))
            {
                try
                {
                    var result = new Words { Word = new List<Word>() };
                    result.Word = wordDict.Select(w => new Word { Text = w.Key, Count = w.Value }).ToList();
                    var xmlSerializer = new XmlSerializer(typeof(Words));
                    var writer = new StreamWriter(targetFilePath);
                    xmlSerializer.Serialize(writer, result);
                    writer.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
            }
            else
            {
                Console.WriteLine("The XML file already exists.");
                return false;
            }
        }

        /// <summary>
        /// this method converts an XML file that compatible with the given format type to given format file type, in this case "Words" class type.
        /// </summary>
        /// <param name="sourceFilePath">XML file path</param>
        /// <returns>this method returns "Words" class type</returns>
        public Words GetWordsFromXmlFile(string sourceFilePath)
        {
            if (File.Exists(sourceFilePath))
            {
                var xmlSerializer = new XmlSerializer(typeof(Words));
                var fs = new FileStream(sourceFilePath, FileMode.Open);
                var obj = (Words)xmlSerializer.Deserialize(fs);
                fs.Close();
                return obj;
            }
            else
            {
                Console.WriteLine("The path or file not found.");
                return null;
            }
        }

        /// <summary>
        /// This method is for word fixing and searching to match with pattern . It uses Regex with given pattern
        /// </summary>
        /// <param name="str">the string to fix or search </param>
        /// <param name="pattern">Regular expression (Regex) pattern</param>
        /// <returns>this method returns string that matched with pattern</returns>
        private string WordFixer(string str, string pattern)
        {
            var word = Regex.Match(str, pattern).Value;
            word = word.Trim('_').TrimEnd(new char[] { '.', '-' });
            return word;
        }       

        /// <summary>
        /// this method returns a "Word" class list that contains tops of  list of "Word" class  which  as the amount given. the list is sorted by descending order relative to the "count" field, and which is being from an XML file (on the given path) that must compatible with the "Words" class. the "Word" class's "count" field is represent count of how often repeated the word. 
        /// </summary>
        /// <param name="count">the count of "How many rows"</param>
        /// <param name="sourceFilePath">XML file's path</param>
        /// <param name="dataRefresh">for caching parameter</param>
        /// <returns>this method returns a List of "Word" class</returns>
        public List<Word> GetTopXListOfWord(int count, string sourceFilePath, bool dataRefresh = false)
        {
            if (File.Exists(sourceFilePath))
            {
                Func<List<Word>> dataFunc = () =>
                    {
                        var fullList = GetWordsFromXmlFile(sourceFilePath);
                        return fullList.Word.OrderByDescending(x => x.Count).Take(count).ToList();
                    };

                var data = _cacheService.GetOrAdd($"Top{count}ListOfWord", dataFunc, dataRefresh);
                return data;
            }
            else
            {
                return new List<Word>() { new Word { Text = "Object not found", Count = 0 } };
            }
        }
    }
}