﻿using Microsoft.Extensions.Caching.Memory;
using System;
using WordCounter.Tools.Interface;

namespace WordCounter.Tools.Implements
{
    public class CacheMemoryService : ICacheService
    {
        private readonly object _cacheLockObj = new object();
        private readonly IMemoryCache _memoryCache;

        public CacheMemoryService(IMemoryCache memoryCache)
        {
            _memoryCache = memoryCache;
        }

        public T Get<T>(string cacheKey)
        {
            _memoryCache.TryGetValue(cacheKey, out T result);
            return result;
        }

        public T GetOrAdd<T>(string cacheKey, Func<T> cacheFunc, bool cacheRefresh = false, int cacheDurationSecond = 300)
        {
            T cacheData = default(T);
            if (!cacheRefresh)
            {
                cacheData = Get<T>(cacheKey);
            }

            if (cacheData == null)
            {
                lock (_cacheLockObj) // to double Check
                {
                    cacheData = cacheFunc();
                    Set(cacheKey, cacheData, cacheDurationSecond);
                    return cacheData;
                }
            }
            return cacheData;
        }

        public void Set<T>(string cacheKey, T obj, int cacheDurationSecond = 300) // default value is 5 mins.
        {
            var cacheEntryOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(cacheDurationSecond));
            _memoryCache.Set(cacheKey, obj, cacheEntryOptions);
        }

        public void Remove(string cacheKey)
        {
            _memoryCache.Remove(cacheKey);
        }
    }
}
