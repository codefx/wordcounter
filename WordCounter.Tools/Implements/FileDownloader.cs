﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Threading;
using WordCounter.Tools.Interface;

namespace WordCounter.Tools.Implements
{
    public class FileDownloader : IFileDownloader
    {
        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(0);
        private bool _result = false;

        public (bool, string message) DownloadFile(string _url, string _fullPathWhereToSave, int timeout = 100000)
        {
            var message = "1";
            if (string.IsNullOrEmpty(_url)) throw new ArgumentNullException("url");
            if (string.IsNullOrEmpty(_fullPathWhereToSave)) throw new ArgumentNullException("fullPathWhereToSave");

            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_fullPathWhereToSave));

                using (WebClient client = new WebClient())
                {
                    if (!File.Exists(_fullPathWhereToSave))
                    {
                        var uri = new Uri(_url);
                        Console.WriteLine(@"Downloading file:");
                        client.DownloadProgressChanged += WebClientDownloadProgressChanged;
                        client.DownloadFileCompleted += WebClientDownloadCompleted;
                        client.DownloadFileAsync(uri, _fullPathWhereToSave);
                        _semaphore.Wait(timeout);
                        return (_result && File.Exists(_fullPathWhereToSave),message);
                    }
                    else
                    {
                        Console.WriteLine("File already exists");
                        return (false,message);
                    }
                }
            }
            catch (Exception e)
            {
                message = "2";
                Console.WriteLine("Was not able to download file!");
                Console.Write(e);
                return (false,message);
            }
            finally
            {
                _semaphore.Dispose();
            }
        }

        private void WebClientDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            Console.Write("\r     -->    {0}%.", e.ProgressPercentage);
        }

        private void WebClientDownloadCompleted(object sender, AsyncCompletedEventArgs args)
        {
            _result = !args.Cancelled;
            if (!_result)
            {
                Console.Write(args.Error.ToString());
            }
            Console.WriteLine(Environment.NewLine + "Download finished!");
            _semaphore.Release();
        }
    }
}
