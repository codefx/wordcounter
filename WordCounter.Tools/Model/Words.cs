﻿using System.Collections.Generic;

namespace WordCounter.Tools.Model
{

    //[XmlRoot(ElementName = "word")]
    public class Word
    {
        //[XmlAttribute(AttributeName = "text")]
        public string Text { get; set; }
        //[XmlAttribute(AttributeName = "count")]
        public int Count { get; set; }
    }

    //[XmlRoot(ElementName = "words")]
    public class Words
    {
        //[XmlElement(ElementName = "word")]
        public List<Word> Word { get; set; }
    }

}
